<h1> Farbod Shabani Player </h1>

This very a minimal player best on the <a href=""> Ali Poormohammadi React Native Code Challenge </a> repo.

## Available Scripts

In the project directory, you can run:

### `yarn start`, `yarn build` ,`yarn eject`

<h2> Used Packages </h2>
<ul>
   <li> <strong>  styling: styled-compontes </strong> </li>
   <li> <strong>  state-management: redux </strong> </li>
</ul>
