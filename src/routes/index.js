import { createBrowserRouter, RouterProvider } from "react-router-dom";

import MainLayout from "layouts/MainLayout";
import { PlayerPage, PlaylistPage } from "pages";

const router = createBrowserRouter([
  {
    path: "/",
    element: <PlaylistPage />,
  },
  {
    path: "/Player/:trackNumber",
    element: <PlayerPage />,
  },
]);

const MainRouter = () => {
  return (
    <MainLayout>
      <RouterProvider router={router} />
    </MainLayout>
  );
};

export default MainRouter;
