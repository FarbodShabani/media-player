import { AppView, AppViewContainer } from "./styled";

const MainLayout = ({ children }) => {
  return (
    // I used this component for app view to be center
    <AppViewContainer>
      <AppView>
        {children}
      </AppView>
    </AppViewContainer>
  );
};

export default MainLayout;
