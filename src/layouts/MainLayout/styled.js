import styled from "styled-components";



const AppViewContainer = styled.div`
width: 100vw;
height: 100vh;
display: flex;
flex-direction: column;
align-items: center;
justify-content: flex-start;
overflow: hidden;
`;

const AppView = styled.div`
max-width: 768px;
width: 100%;
height: 100vh;
display: flex;
flex-direction: column;
justify-content: flex-start;
align-items: center;
padding: 5px 5px 0px;
overflow: hidden;
`;




export {
    AppView,
    AppViewContainer,
}