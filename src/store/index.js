import { configureStore } from '@reduxjs/toolkit'

import rateSlice from './slice/rateSlice'
import  favouriteSlice  from './slice/favourite'

export default configureStore({
  reducer: {
    rates: rateSlice,
    favourites: favouriteSlice,
  },
})