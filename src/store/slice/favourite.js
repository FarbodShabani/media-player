import { createSlice } from '@reduxjs/toolkit'

import playlistData from "data/playlist.json"

// creating initial state based on the fake data
const setInitState = () => {
  const initFavourite = {};
  playlistData.data.map((d, index) => initFavourite[index] = {value : false});
  return initFavourite;
}

export const favouriteSlice = createSlice({
  name: 'favourites',
  initialState: {
    value: setInitState(),
  },
  reducers: {
    changeFavouriteStatus: (state, action) => {
      const {trackNumber, value} = action.payload;
      const newFavouriteList = setInitState();
      newFavouriteList[`${trackNumber}`].value = value;
      state.value =  newFavouriteList;
    },
  },
});

// Action creators are generated for each case reducer function
export const { changeFavouriteStatus } = favouriteSlice.actions;

export default favouriteSlice.reducer