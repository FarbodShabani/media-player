import { createSlice } from '@reduxjs/toolkit'

import playlistData from "data/playlist.json"

const setInitState = () => {
  const initRates = {};
  playlistData.data.map((d, index) => initRates[index] = {value : 0});
  return initRates;
}

export const rateSlice = createSlice({
  name: 'rates',
  initialState: {
    value: setInitState(),
  },
  reducers: {
    changeTrackRate: (state, action) => {
      const {trackNumber, newRate} = action.payload;
      const newRates = state.value;
      newRates[trackNumber] = {value: newRate};
      state.value = newRates;
    },
  },
});

// Action creators are generated for each case reducer function
const { changeTrackRate } = rateSlice.actions;

export {
  changeTrackRate,
}

export default rateSlice.reducer