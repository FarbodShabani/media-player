import { createSelector } from "reselect";

const selectRates = (state) => state.rates.value;

// this will give you a lists of rates
const getRates = createSelector(selectRates, (rates) => rates);

// this will give you a rate status of single track based on track number
const getRate = (trackNumber) =>
  createSelector(selectRates, (rates) => rates[trackNumber]);

export { getRates, getRate };
