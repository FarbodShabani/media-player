import { createSelector } from "reselect";

const selectFavourites = (state) => state.favourites.value;

// this reselect will give the list of fabourites track status
const getFavourites = createSelector(selectFavourites, (favourites) => favourites);

// this will give you a single track favourite status based on the track number
const getFavourite = (trackNumber) =>
  createSelector(selectFavourites, (favourites) => favourites[trackNumber]);

export { getFavourites, getFavourite };
