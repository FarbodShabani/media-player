import { PlaylistTrack, Title } from "components";
import { Scrolable, TrackContainer } from "./styled";

import PlaylistData from "data/playlist.json"
import { useNavigate } from "react-router-dom";

const PlaylistPage = () => {

  const navigate = useNavigate();

    return (
    <>
      <Title type={"header"}>Skoovin'</Title>
      <Scrolable>
        {PlaylistData?.data.map((track, index) => (
          <TrackContainer key={index} onClick={() => navigate("/player/"+index)}>
            <PlaylistTrack trackDetail={track} trackNumber={index} />
          </TrackContainer>
        ))}
      </Scrolable>
    </>
  );
};

export default PlaylistPage;
