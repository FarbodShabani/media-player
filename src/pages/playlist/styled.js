import styled from "styled-components";




const Scrolable = styled.div`
   width: 100%;
   height: 90vh; 
   overflow-x: hidden;
   overflow-y: scroll;
   display: flex;
   flex-direction: column;
   justify-content: flex-start;
   align-items: center;

   &::-webkit-scrollbar {
    width: 12px;
    border: 1px solid #cfcfcf;
   }

   &::-webkit-scrollbar-thumb {
    border-radius: 5px;
    background-color: #cccccc;
   }

   &::-webkit-scrollbar-thumb:hover {
   background: #7c7c7c; 
   }
`;

const TrackContainer = styled.div`
   width: 90%;
   height: 250px;
   margin: 20px 0px;
   position: relative;
   display: flex;
   flex-direction: column;
   justify-content: flex-start;
   align-items: center;
   border: 1px solid #cfcfcf;
`;



export {
    Scrolable,
    TrackContainer,
}