import { useState } from "react";
import { useParams } from "react-router-dom";

import { AudioPlayer, PlayerAlbumArt, Rating, Title } from "components";

import { AlbumArtContainer, RatingContainer, SliderContainer, SliderInput, TimerContainer } from "./styled";

import { secondToMinSec } from "utils/converter/Time";

import playlistData from "data/playlist.json";

const PlayerPage = () => {

  const {trackNumber} = useParams();

  const { title, cover, audio, totalDurationMs: DBDuration } = playlistData.data[trackNumber];

  const [sliderValue, setSliderValue] = useState(0);
  const [playStatus, setPlayStatus] = useState(false);

  return (
    <>
      <Title type={"header"}>{title}</Title>
      <AlbumArtContainer>
        <PlayerAlbumArt
          trackNumber={trackNumber}
          trackCover={cover}
          trackTitle={title}
          playStatus={playStatus}
          setPlayStatus={setPlayStatus}
        />
      </AlbumArtContainer>
      <SliderContainer>
        <SliderInput
          type="range"
          value={sliderValue}
          max={DBDuration / 1000}
          onChange={(e) => setSliderValue(e.target.value)}
        />
      </SliderContainer>
      <AudioPlayer
        audioFile={audio}
        play={playStatus}
        newCurrentTime={sliderValue}
        setNewCurrentTime={(e) => setSliderValue(e)}
      />
      <TimerContainer>
        <Title>
          {secondToMinSec(sliderValue)}/{secondToMinSec(DBDuration / 1000)}
        </Title>
      </TimerContainer>
      <RatingContainer>
        <Rating trackNumber={trackNumber} interactive/>
      </RatingContainer>
    </>
  );
};

export default PlayerPage;
