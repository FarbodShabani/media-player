import styled from "styled-components"

import { albumArtLength } from "utils/style/length";

const AlbumArtContainer = styled.div`
   width: ${albumArtLength}px;
   height: ${albumArtLength}px;
   margin: 5vh;
   position: relative;
`

const SliderContainer = styled.div`
   width: ${albumArtLength}px;
   height: 2vh;
   margin: 5vh;
   display: flex;
   justify-content: center;
   align-items: center;
`;

const SliderInput = styled.input`
 width: 100%;
 `;
 
 const TimerContainer = styled.div`
    width: 100%;
    height: 5vh;
    display: flex;
    justify-content: center;
    align-items: center;
 `;

 const RatingContainer = styled.div`
    width: ${albumArtLength}px;
    height: 70px;
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 0px auto;
 `;



export {
    AlbumArtContainer,
    SliderContainer,
    SliderInput,
    TimerContainer,
    RatingContainer
}