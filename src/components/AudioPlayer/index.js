import { useEffect, useRef } from "react";




const AudioPlayer = ({audioFile, play, newCurrentTime, setNewCurrentTime}) => {

    // use this ref to access to the audio tage
    const audioElem = useRef();

    // use this useEffect to control play and pause based on play status 
    useEffect(() => {
        if (play) {
            audioElem.current.play();
        } else {
            audioElem.current.pause();
        }
    }, [play])

    // this useEffect to control seek
    // the reason for using this condition `Math.abs(newCurrentTime - audioElem.current.currentTime) > 1` is that I tried
    // to make sure both audio and slider value have the same value but when I was tring to make this connection
    // I found out that with every change of the slider the useEffect tries to set the current time of the audio. So I wrote
    // this condition to prevent that.
    useEffect(() => {
        if (Math.abs(newCurrentTime - audioElem.current.currentTime) > 1)  audioElem.current.currentTime =  newCurrentTime;
    }, [newCurrentTime])

    // this fuction update slider value based on the audio current time.
    const onPlayingTimeUpdate = () => {
        setNewCurrentTime(audioElem.current.currentTime)
    }

    return (
        <audio src={audioFile} ref={audioElem} onTimeUpdate={onPlayingTimeUpdate}/>
    )
}




export default AudioPlayer;