import { HeaderContainer, HeaderTitle, SmallTitle } from "./styled";

const Title = ({ type, children }) => {
  if (type === "header") {
    return (
      <HeaderContainer>
        <HeaderTitle>{children}</HeaderTitle>
      </HeaderContainer>
    );
  }
  return <SmallTitle>{children}</SmallTitle>;
};

export default Title;
