import styled from "styled-components";



// header style
const HeaderContainer = styled.div`
   height: 10vh;
   width: 100vw;
   display: flex;
   flex-direction: column;
   justify-content: flex-end;
   align-items: center;
`;

const HeaderTitle = styled.h1`
   font-family: Poppins;
   margin: 0px;
   text-align: center;
`;

// small Title

const SmallTitle = styled.h3`
   font-family: Poppins;
   margin: 0px;
   text-align: center;
`;


export {
    HeaderTitle,
    HeaderContainer,
    SmallTitle,
}