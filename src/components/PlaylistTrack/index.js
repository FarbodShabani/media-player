import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";

import { getFavourite } from "store/reselect/favourite";
import { changeFavouriteStatus } from "store/slice/favourite";

import TrackShape from "utils/shapes/TrackShape";

import Title from "components/Title";
import Rating from "components/Rating";

import {
  FavouriteIcon,
  TrackBottomContainer,
  TrackRatingContainer,
  TrackThumbNail,
} from "./styled";

import favouriteFilled from "assets/icons/favourite/heart-filled-black.png";
import favouriteLine from "assets/icons/favourite/heart-line-black.png";

const PlaylistTrack = ({ trackDetail, trackNumber }) => {
  const dispatch = useDispatch();
  const {value : favouriteValue} = useSelector(getFavourite(trackNumber));


  return (
    <>
      <TrackRatingContainer>
        <Rating trackNumber={trackNumber} />
      </TrackRatingContainer>
      <TrackThumbNail alt={trackDetail.title} src={trackDetail.cover} />
      <TrackBottomContainer onClick={(e) => e.stopPropagation()}>
        <Title>{trackDetail.title}</Title>
        <FavouriteIcon
          src={favouriteValue ? favouriteFilled : favouriteLine}
          alt="Favourite Icon"
          onClick={() => dispatch(changeFavouriteStatus({trackNumber, value: !favouriteValue}))}
        />
      </TrackBottomContainer>
    </>
  );
};

PlaylistTrack.prototype = {
  trackDetail: TrackShape.isRequired,
  trackNumber: PropTypes.number.isRequired,
};

export default PlaylistTrack;
