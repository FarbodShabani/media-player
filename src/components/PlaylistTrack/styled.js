import styled from "styled-components";



const TrackThumbNail = styled.img`
   width: 100%;
   height: 75%;
   object-fit: cover;
`;

const TrackRatingContainer = styled.div`
   position: absolute;
   top: 10px;
   left: 10px;
   width: 150px;
   height: 30px;
   display: flex;
   flex-direction: row;
   justify-content: center;
   align-items: center;
`;

const TrackBottomContainer = styled.div`
   width: 100%;
   height: 25%;
   display: flex;
   flex-direction: row;
   justify-content: center;
   align-items: center;
   position: relative;
`;

const FavouriteIcon = styled.img`
   width: 25px;
   height: 25px;
   position: absolute;
   right: 5px;
`;




export {
    TrackThumbNail,
    TrackRatingContainer,
    TrackBottomContainer,
    FavouriteIcon
}