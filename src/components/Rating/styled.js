import styled from "styled-components";



const RatingConatainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const RateIcon = styled.img`
  width: 20%;
  height: 100%;
`;

export { 
    RatingConatainer,
    RateIcon,
 };
