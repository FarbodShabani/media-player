import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import { getRate } from "store/reselect/star";
import { changeTrackRate } from "store/slice/rateSlice";

import { RateIcon, RatingConatainer } from "./styled";

import fillStarIconImage from "assets/icons/stars/star-filled-black.png";
import lineStarIconImage from "assets/icons/stars/star-line-black.png";

const starList = [1, 2, 3, 4, 5];
const Rating = ({ trackNumber, interactive }) => {

  const dispatch = useDispatch();
  const rate = useSelector(getRate(trackNumber)).value;

  // this function will change the track music rate and check that if you can change the rate here or not
  const onGetNewRate = (newRate) => {
    if (interactive) {
      dispatch(changeTrackRate({trackNumber, newRate}))
    }
  }

  return (
    <RatingConatainer>
      {starList.map((star) => (
        <RateIcon
          key={star}
          alt="rate icon"
          src={star <= rate ? fillStarIconImage : lineStarIconImage}
          onClick={() => onGetNewRate(star)}
          onTouchStart={() => onGetNewRate(star)}
          onMouseEnter={() => onGetNewRate(star)}
          disabled
        />
      ))}
    </RatingConatainer>
  );
};

Rating.prototype = {
  trackNumber: PropTypes.number.isRequired,
  interactive: PropTypes.bool,
};

export default Rating;
