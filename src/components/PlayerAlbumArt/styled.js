import styled from "styled-components";

import { albumArtLength, playPauseIconLength } from "utils/style/length";




const playPauseIconPosition = (albumArtLength / 2) - (playPauseIconLength / 2);



const PlayPauseIcon = styled.img`
width: ${playPauseIconLength}px;
height: ${playPauseIconLength}px;
position: absolute;
top: ${playPauseIconPosition}px;
left: ${playPauseIconPosition}px;
background-color: white;
border-radius: 180px;
object-fit: contain;
`;


const FavouriteIcon = styled.img`
   width: 50px;
   height: 50px;
   position: absolute;
   right: 10px;
   bottom: 10px;
`;

const ArtAlbumImage = styled.img`
   width: 100%;
   height: 100%;
`;





export {
    PlayPauseIcon,
    FavouriteIcon,
    ArtAlbumImage
}