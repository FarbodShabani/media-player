import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';

import { getFavourite } from 'store/reselect/favourite';
import { changeFavouriteStatus } from 'store/slice/favourite';

import { ArtAlbumImage, FavouriteIcon, PlayPauseIcon } from "./styled";




import favouriteFilled from "assets/icons/favourite/heart-filled-black.png";
import favouriteLine from "assets/icons/favourite/heart-line-black.png";
import playIcon from "assets/icons/playerIcon/play.png";
import PauseIcon from "assets/icons/playerIcon/pause.png";


const PlayerAlbumArt = ({trackCover, trackNumber, trackTitle, playStatus, setPlayStatus}) => {
    
  const dispatch = useDispatch();
  const { value: favouriteValue } = useSelector(getFavourite(trackNumber));

  return (
    <>
      <PlayPauseIcon
        alt="play pause button"
        src={playStatus ? PauseIcon : playIcon}
        onClick={() => setPlayStatus(!playStatus)}
      />
      <ArtAlbumImage
          src={trackCover}
          alt={trackTitle}
        />
      <FavouriteIcon
          src={favouriteValue ? favouriteFilled : favouriteLine}
          alt="Favourite Icon"
          onClick={() =>
            dispatch(
              changeFavouriteStatus({ trackNumber, value: !favouriteValue })
            )
          }
        />
    </>
  );
};

PlayerAlbumArt.prototype = {
    trackCover: PropTypes.string.isRequired,
    trackTitle: PropTypes.string.isRequired,
    trackNumber: PropTypes.number.isRequired,
    playStatus: PropTypes.bool.isRequired,
    setPlayStatus: PropTypes.func.isRequired,
  };




export default PlayerAlbumArt;