import PropTypes from "prop-types"

export default PropTypes.shape({
    title : PropTypes.string,
    audio: PropTypes.string,
    cover: PropTypes.string,
    totalDurationMs: PropTypes.number, 
})