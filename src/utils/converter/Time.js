


// generating Minute and second  based on the total second
const secondToMinSec = (totalSecond) => {
  const Min = Math.floor(totalSecond  / 60 );
  const Sec = Math.floor(totalSecond  - Min * 60);
  const stringMin = Min > 10 ? Min : "0"+Min;
  const stringSec = Sec > 10 ? Sec : "0"+Sec;
  return stringMin + ":" + stringSec;
};




export {
    secondToMinSec,
}